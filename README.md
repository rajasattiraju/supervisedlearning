# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

In this Notebook, we are going to analyze and evaluate the performance of supervised machine learning classifiers from the Python's Scikit library. The input data are the radio scans from the Time Domain Monostatic Radar module.

## Time Domain Monostatic Radar
TD Monostatic Radar is a fully-coherent, short-range monostatic radar sensor with a RF bandwidth of 1.4 GHz and a centre frequency of 4.3 GHz. as shown in the figure.

![TDP440](tdp440.png)

It can be used to collect the raw radar scans (amplitude or reflectivity) as a function of time in nanoseconds. These can be logged into csv files. A Waveform log for a timestep consists of the following information

1. Time stamp (PC-generated timestamp)
2. Message name
3. Config Info
4. Raw Scan Data - Raw signal without the use of any filters
5. Bandpass Data - data after bandpass filtering using a third order filter
6. Motion Filtered Data - using 4 filters
7. Detection Data - based on the motion filtered data

These data are highlighted in the below figure

![MRMScan](mrmscan.png)

## Machine Learning 
Add content about ML here

## Objective of this work
Since the TD modules provide high resolution radar scans over short distances, they can be used for detecting people or other obstacles for UCT2: Automatic Train pairing Use case for the project SBDist. Since each train will be equipped with two SBDist modules, one of then can be used in Ranging Radio (RR) mode to establish communication and measuring the distance between the two trains as the two trains slowly come closer for mechanical pairing. The second SBDist module can be used as a MR scanning the environment around to detect any people / obstacles in risk of collision.

By using the scan data and the Machine Learning algorithms (both supervised and unsupervised), it may be possible to evaluate the presence/absence of an obstacle as a classic classification problem. However, this requires the availability of labelled training data. 

## The Training Data
The realistic training data can only be collected by installing the TD modules in some trains and collecting the data during real world coupling scenarios. We can progressively get there by starting with simple scenarios.

### Scenario 1: Static TD module in a room with and without a Person
In this scenario, the TD module was put in an empty room and left to record the radio scans as follows.

1. Scans collected with No persons standing in and around the module
2. Scans collected with one person standing and moving around the module

The scans were collected and stored in the directory mrmlog/OfficeLogs and named accordingly

## List of Classifiers being used
The following list of classifiers are planned to be evaluated. Please check [this page](http://scikit-learn.org/stable/user_guide.html) for more details
### Generalized Linear Models
1. Logistic Regression
2. Perceptron

### Nearest neighbors
1. k-Nearest neighbors classifier
2. Radius neighbors classifier
3. Nearest centroid classifier

### Support Vector Machines
1. LinearSVC

### Ensemble based Methods
* Bagging Algorithms
    1. Decision Trees
    2. Random Forest
    3. Extra Tree Classifier    
* Boosting Algorithms
    1. AdaBoost
    2. Stochastic Gradient Boost 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact